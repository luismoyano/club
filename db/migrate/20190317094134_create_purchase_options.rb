class CreatePurchaseOptions < ActiveRecord::Migration[5.2]
  def change
    create_table :purchase_options do |t|
      t.belongs_to :title, polymorphic: true, index: true
      t.belongs_to :price, index: true
      t.timestamps
    end
  end
end
