class CreatePurchases < ActiveRecord::Migration[5.2]
  def change
    create_table :purchases do |t|
      t.belongs_to :title, polymorphic: true, index: true
      t.belongs_to :user, index: true

      t.timestamps
    end
  end
end
