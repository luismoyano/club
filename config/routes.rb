Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :movies, only: [:index]
  resources :seasons, only: [:index]
  post 'purchases/:user_id', to: 'purchases#create'

  resources :titles, only: [:index, :show] do
    resources :purchase_options, only: [:show]
  end

  resources :users, only: [:show] do
    resources :library, only: [:index], to: 'users#library_index'
  end

end
