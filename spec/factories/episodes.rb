FactoryBot.define do
  factory :episode do
    name { Faker::Alphanumeric.alphanumeric 20 }
    plot { Faker::Alphanumeric.alphanumeric 500 }
    episode_number { episode_number }
    season
  end
end
