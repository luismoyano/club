FactoryBot.define do
  factory :season do
    name { Faker::Alphanumeric.alphanumeric 20 }

    factory :season_with_episodes
      transient do
        episodes_count { 15 }
        ep_num { Faker::Number.within(1..15) }
      end

      after(:create) do |season, evaluator|
        create_list(:episode, evaluator.episodes_count,
          season: season,
          episode_number: evaluator.ep_num)
      end


  end
end
