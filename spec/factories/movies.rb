FactoryBot.define do
  factory :movie do
    name { Faker::Alphanumeric.alphanumeric 20 }
    plot { Faker::Alphanumeric.alphanumeric 500 }
  end
end
