FactoryBot.define do
  factory :price do
    description { Faker::Alphanumeric.alphanumeric 20 }
    cost { 2.99 }
  end

  factory :purchase_option do
  end
end
