require 'rails_helper'

#Titles (movies & Seasons)
RSpec.describe 'Titles API', type: :request do

  let!(:movies) { create_list(:movie, 50) }
  let!(:seasons) { create_list(:season, 50) }

  describe 'Get titles sorted by creation' do

    # HTTP get request
    before { get '/titles' }

    it 'returns movies sorted by creation' do
      expect(json).not_to be_empty
      expect(json.size).to eq(2)
      expect(json['movies'].size).to eq(50)
      expect(json['seasons'].size).to eq(50)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end
end
