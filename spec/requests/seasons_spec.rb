require 'rails_helper'

#Seasons
RSpec.describe 'Seasons API', type: :request do

  let!(:seasons) { create_list(:season_with_episodes, 100) }

  describe 'Get seasons with episodes sorted by ep number' do

    # HTTP get request
    before { get '/seasons'}

    it 'returns seasons and episodes' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(100)
      expect(json[0][2].size).to be > 0
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end
end
