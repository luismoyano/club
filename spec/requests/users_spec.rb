require 'rails_helper'

RSpec.describe 'Users API', type: :request do

  let!(:user) { create(:user) }

  let!(:movie) { create(:movie) }
  let!(:purchased_movie) { create(:purchase, title: movie, user: user) }

  let!(:season) { create(:season) }
  let!(:purchased_season) { create(:purchase, title: season, user: user) }

  let!(:not_rented_season) { create(:season) }

  let(:expired_season) { create (:season) }
  let!(:expired_season_purchase) { create(:purchase, title: expired_season, user: user, updated_at: Time.now + 4.days) }


  describe 'Get users library' do

    let(:attrs) { { sort_by: 'expiry_asc' } }

    # HTTP get request
    before { get "/users/#{user.id}/library/",  params: attrs }

    it 'returns the titles the user has purchased' do
      expect(json).not_to be_empty
      expect(json.size).to eq(2)

    end

    it 'doesnt provide the titles the user has not purchased' do
      expect(json[0]['title_id']).not_to eq(not_rented_season.id)
      expect(json[1]['title_id']).not_to eq(not_rented_season.id)
    end

    it 'doesnt provide the titles the user has purchased and have expired' do
      expect(json[0]['title_id']).not_to eq(expired_season.id)
      expect(json[1]['title_id']).not_to eq(expired_season.id)
    end

  end

end
