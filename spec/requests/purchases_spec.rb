require 'rails_helper'

RSpec.describe 'Purchases API', type: :request do

  let!(:movie) { create(:movie) }
  let!(:season) { create(:season) }
  let!(:price) { create(:price) }
  let!(:purchase_option_movie) { create(:purchase_option, title: movie, price: price) }
  let!(:purchase_option_season) { create(:purchase_option, title: season, price: price) }

  let!(:user) { create(:user) }

  let!(:owned_movie) { create(:movie) }
  let!(:purchase_option_owned_movie) { create(:purchase_option, title: owned_movie, price: price) }
  let!(:purchased_owned_movie) { create(:purchase, title: owned_movie, user: user) }

  let!(:owned_season) { create(:season) }
  let!(:purchase_option_owned_season) { create(:purchase_option, title: owned_season, price: price) }
  let!(:purchased_owned_season) { create(:purchase, title: owned_season, user: user) }

  describe 'perform the purchase of a movie' do

    context 'We ask for a purchase option on a specific movie' do
      let(:attrs) { { type: 'Movie' } }

      # HTTP get request
      before { get "/titles/#{movie.id}/purchase_options/#{purchase_option_movie.id}",  params: attrs }

      it 'returns a purchase option' do
        expect(json).not_to be_empty
        expect(json[0]['id']).to eq(purchase_option_movie.id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'We request to purchase the specified movie' do

      let(:attrs) { { purchase_option: purchase_option_movie.id } }
      before { post "/purchases/#{user.id}/", params: attrs }

      it 'returns the succesful purchase' do
        expect(json).not_to be_empty
        expect(json['title_id']).to eq(movie.id)
        expect(json['title_type']).to eq('Movie')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end

    end

    context 'We request to purchase the specified season' do

      let(:attrs) { { purchase_option: purchase_option_season.id } }
      before { post "/purchases/#{user.id}/", params: attrs }

      it 'returns the succesful purchase' do
        expect(json).not_to be_empty
        expect(json['title_id']).to eq(season.id)
        expect(json['title_type']).to eq('Season')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end

    end

    context 'We request to purchase a movie we already have' do

      let(:attrs) { { purchase_option: purchase_option_owned_movie.id } }
      before { post "/purchases/#{user.id}/", params: attrs }

      it 'returns the purchase' do
        expect(json).not_to be_empty
        expect(json['title_id']).to eq(owned_movie.id)
        expect(json['title_type']).to eq('Movie')
      end

      it 'returns status code 400' do
        expect(response).to have_http_status(400)
      end

    end

    context 'We request to purchase a season we already have' do

      let(:attrs) { { purchase_option: purchase_option_owned_season.id } }
      before { post "/purchases/#{user.id}/", params: attrs }

      it 'returns the purchase' do
        expect(json).not_to be_empty
        expect(json['title_id']).to eq(owned_season.id)
        expect(json['title_type']).to eq('Season')
      end

      it 'returns status code 400' do
        expect(response).to have_http_status(400)
      end

    end

  end
end
