require 'rails_helper'

#Films
RSpec.describe 'Movies API', type: :request do

  let!(:movies) { create_list(:movie, 100) }

  describe 'Get movies sorted by creation' do
    #valid params
    let(:attrs) { { sort_method: 'date_desc' } }

    # HTTP get request
    before { get '/movies',  params: attrs }

    it 'returns movies sorted by creation' do
      expect(json).not_to be_empty
      expect(json.size).to eq(100)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end
end
