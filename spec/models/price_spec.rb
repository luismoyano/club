require 'rails_helper'

RSpec.describe Price, type: :model do
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:cost) }

  it { should validate_length_of(:description) }
  it { should validate_numericality_of(:cost) }

  #Test associations
  it { should have_many(:movies) }
  it { should have_many(:seasons) }
  it { should have_many(:purchase_options) }
end
