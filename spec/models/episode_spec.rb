require 'rails_helper'

RSpec.describe Episode, type: :model do

  #Validations
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:plot) }
  it { should validate_presence_of(:episode_number) }

  it { should validate_length_of(:name) }
  it { should validate_length_of(:plot) }
  it { should validate_numericality_of(:episode_number) }

  #Test associations
  it { should belong_to(:season) }

end
