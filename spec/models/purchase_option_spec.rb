require 'rails_helper'

RSpec.describe PurchaseOption, type: :model do
  it { should belong_to(:title) }
  it { should belong_to(:price) }
end
