require 'rails_helper'

RSpec.describe Movie, type: :model do

  #Validations
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:plot) }

  it { should validate_length_of(:name) }
  it { should validate_length_of(:plot) }

  #Test associations
  it { should have_many(:prices) }
  it { should have_many(:purchase_options) }
  it { should have_many(:users) }
  it { should have_many(:purchases) }
end
