require 'rails_helper'

RSpec.describe User, type: :model do

  #Validations
  it { should validate_presence_of(:email) }

  #format
  it { should allow_value("email@foo.bar").for(:email) }
  it { should_not allow_value("foo/bar").for(:email) }

  #Test associations
  it { should have_many(:purchases) }
  it { should have_many(:movies) }
  it { should have_many(:seasons) }
end
