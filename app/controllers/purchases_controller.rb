class PurchasesController < ApplicationController

  def create

    @user = User.find(params[:user_id])

    @option = PurchaseOption.find(params[:purchase_option])
    @purchase = @user.purchases.find_by(title: @option.title)

    @res_code = :created

    if(@purchase.blank?)
      @purchase = @user.purchases.create(title: @option.title)
    elsif !@purchase.blank? && @purchase.updated_at > Time.now + Purchase::VALID_TIME
      @purchase.updated_at = Time.now
    else
      @res_code = :bad_request
    end
    json_response(@purchase, @res_code)
  end

end
