class MoviesController < ApplicationController

  SORT_BY_DATE_DESC = 'date_desc'

  def index
    if(params[:sort_method] = SORT_BY_DATE_DESC)
      @movies = Movie.order({ created_at: :desc })
    else
      @movies = Movie.all
    end
    json = Rails.cache.fetch('Seasons') do
        @movies
      end
    json_response(json)
  end

end
