class UsersController < ApplicationController

  SORT_BY_EXPIRY_ASC = "expiry_asc"

  def library_index
    @user = User.find(params[:user_id])

    @purchases = @user.purchases.where("updated_at < ?", Time.now + Purchase::VALID_TIME)
    if(params[:sort_by] == SORT_BY_EXPIRY_ASC)
      @purchases.order(updated_at: :asc)
    end

      json_response(@purchases)
  end

end
