class ApplicationController < ActionController::API
  include Response

  #method to fake authenticated user
  def current_user
    @email = "luis@club.com"
    @user = User.first_or_create!(:email => @email)
    return @user
  end

end
