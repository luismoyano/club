class TitlesController < ApplicationController

  def index

    @movies = Movie.order({ created_at: :desc})
    @seasons = Season.order({ created_at: :desc})

    json = Rails.cache.fetch('titles') do
      {
        :movies => @movies,
        :seasons => @seasons
      }
    end
    render :json => json

  end

end
