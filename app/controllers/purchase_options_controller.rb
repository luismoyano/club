class PurchaseOptionsController < ApplicationController

  def show
    json_response(PurchaseOption.where({id: params[:id]}))
  end

end
