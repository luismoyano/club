class SeasonsController < ApplicationController

  SORT_BY_DATE_DESC = 'date_desc'

  def index

    if(params[:sort_method] = SORT_BY_DATE_DESC)
      @seasons = Season.order({ created_at: :desc })
    else
      @seasons = Season.all
    end

    @season_hash = []


    @seasons.each{|s|
      @s_hash = []
      @s_hash << s.name
      @s_hash << s.created_at

      @eps_hash = []

      s.episodes.order({episode_number: :asc}).each{|e|
        @e_hash = []
        @e_hash << e.name
        @e_hash << e.plot
        @e_hash << e.episode_number

        @eps_hash << @e_hash
      }

      @s_hash << @eps_hash
      @season_hash << @s_hash
    }

    json = Rails.cache.fetch('Seasons') do
      @season_hash.to_json
    end

    render :json => json
  end
end
