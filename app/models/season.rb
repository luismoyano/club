class Season < ApplicationRecord
  validates :name, presence: true, length: {in: 1..30}

  has_many :purchase_options, as: :title
  has_many :prices, through: :purchase_options

  has_many :episodes

  def as_json(options)
    options = { :only => [:name, :created_at] }.merge(options)
    super(options)
  end

end
