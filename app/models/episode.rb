class Episode < ApplicationRecord
  validates :name, presence: true, length: {in: 1..30}
  validates :plot, presence: true, length: {in: 10..600}
  validates :episode_number, presence: true, numericality: { greater_than_or_equal_to: 0 }

  belongs_to :season

  def as_json(options)
    options = { :only => [:name, :plot, :episode_number] }.merge(options)
    super(options)
  end
end
