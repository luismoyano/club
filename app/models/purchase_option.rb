class PurchaseOption < ApplicationRecord
  belongs_to :title, polymorphic: true
  belongs_to :price
end
