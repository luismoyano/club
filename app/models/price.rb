class Price < ApplicationRecord
  validates :description, presence: true, length: {in: 10..600}
  validates :cost, presence: true, numericality: { greater_than: 0 }
  validates :quality, inclusion: { in:[@sd_quality, @hd_quality] }


  has_many :purchase_options
  has_many :movies, through: :purchase_options, source: :title, source_type: 'Movie'
  has_many :seasons, through: :purchase_options, source: :title, source_type: 'Season'


  @sd_quality = "SD"
  @hd_quality = "HD"

  HD_QUALITY = @hd_quality.freeze
  SD_QUALITY = @sd_quality.freeze
end
