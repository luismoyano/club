class User < ApplicationRecord
  validates :email, presence: true, format: { with: /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/ }


  has_many :purchases
  has_many :movies, through: :purchases, source: :title, source_type: 'Movie'
  has_many :seasons, through: :purchases, source: :title, source_type: 'Season'

end
