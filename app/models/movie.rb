class Movie < ApplicationRecord
  validates :name, presence: true, length: {in: 2..30}
  validates :plot, presence: true, length: {in: 10..600}

  has_many :purchase_options, as: :title
  has_many :prices, through: :purchase_options

  has_many :purchases, as: :title
  has_many :users, through: :purchases

  def as_json(options)
    options = { :only => [:name, :plot, :created_at] }.merge(options)
    super(options)
  end

end
